﻿using UnityEngine;
using System.Collections;

public class MainController : MonoBehaviour
{

    public float maxEnemies = 2;
    public float difficulty = 0.05f;
    public Rigidbody enemy;
    public Vector3 spawn = new Vector3(2, 0.2f, 4);

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        int currEnemies = GameObject.FindGameObjectsWithTag("Enemy").Length;


        if (Input.GetKey("escape"))
            Application.Quit();

        if (currEnemies < maxEnemies)
        {
            float f = Random.value;
            if (f <= difficulty)
            {
                Debug.Log("Novo Inimigo");
                Rigidbody bulletClone = (Rigidbody)Instantiate(enemy, transform.position, transform.rotation);
            }

        }

    }
}

