﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Player : MonoBehaviour {


    public float hp = 3;
    public float timeBetweenDamage = 10;
    float maxHp = 3;
    bool invulnerable = false;
    float lastAttacked;


    public Vector2 hpBarPos = new Vector2(20, 40);
    public Vector2 hpBarSize = new Vector2(100, 20);
    public Texture2D emptyTex;
    public Texture2D fullText;

    public float m_Speed;
    public float m_TurnSpeed = 180f;

    private Rigidbody m_Rigidbody;

    private float m_MovementInputValue;
    private float m_TurnInputValue;

    void OnGUI()
    {
        //draw the background:
        GUI.BeginGroup(new Rect(hpBarPos.x, hpBarPos.y, hpBarSize.x, hpBarSize.y));
        GUI.Box(new Rect(0, 0, hpBarSize.x, hpBarSize.y), emptyTex);
        //draw the filled-in part:
        GUI.BeginGroup(new Rect(0, 0, hpBarSize.x * (hp/maxHp), hpBarSize.y));
        GUI.Box(new Rect(0, 0, hpBarSize.x, hpBarSize.y), fullText);
        GUI.EndGroup();
        GUI.EndGroup();

    }

    // Use this for initialization
    void Start () {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_MovementInputValue = 0;
        m_TurnInputValue = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (Time.time - lastAttacked > 3 && invulnerable)
            invulnerable = false;

        m_MovementInputValue = Input.GetAxis("Vertical");
        m_TurnInputValue = Input.GetAxis("Horizontal");
    }

    void FixedUpdate()
    {

        Move();
        Turn();
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            GameObject.Find("Weapon").GetComponent<Shooting>().incrementAmmo();
            Destroy(col.gameObject);
        }
        else if(col.gameObject.tag == "Enemy" && !invulnerable)
        {
            takeDamage();
        }
        else if (col.gameObject.tag == "Food")
        {
            if(hp < maxHp)
                heal();
            else
                GameObject.Find("Weapon").GetComponent<Shooting>().incrementAmmo();

            Destroy(col.gameObject);
        }
    }

    void takeDamage()
    {
        lastAttacked = Time.time;
        invulnerable = true;
        hp -= 1;

        if (hp <= 0) {
            SceneManager.LoadScene("LooseMenu");
        }
    }

    void heal()
    {
        hp += 1;
    }

    private void Move()
    {
        // Adjust the position of the tank based on the player's input.
        Vector3 movement = transform.forward * m_MovementInputValue * m_Speed * Time.deltaTime;

        m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
    }


    private void Turn()
    {
        // Adjust the rotation of the tank based on the player's input.
        float turn = m_TurnInputValue * m_TurnSpeed * Time.deltaTime;

        Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);

        m_Rigidbody.MoveRotation(m_Rigidbody.rotation * turnRotation);
    }
}
