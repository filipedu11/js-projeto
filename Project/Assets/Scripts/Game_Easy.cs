﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Game_Easy : MonoBehaviour {

    public GameObject grass;
    public GameObject tree1;
    //public GameObject stone;
    public GameObject happy_meal;
    public GameObject macdonalds;
    public GameObject enemy;
    public GameObject roller;

    public Transform player;

    //Identificadores do coteudo para cada parte do terreno
    private static int nothing_key = 0;
    private static int tree_key = 1;
    private static int happy_meal_key = 2;
    private static int macdonalds_key = 3;
    private static int path_key = 4;
    private static int enemy_key = 5;

    //Tamanho do terreno
    private int sizeOfTerrain = 100;

    //Tamanho que os objectos ocupam
    private int start_point = 4;

    //Posição inicial do jogador
    private int posX = 5;
    private int posZ = 5;

    //Matriz que mostra o conteudo de cada posição do mapa
    private int[,] terrain = new int[100,100];

    //Numero maximo de inimigos e happy meals
    private int max_enemy = 10;
    private int max_happy_meals = 5;

    // Use this for initialization
    void Start() {

        //NOTA IMPORTANTE
        //A variavel j nos ciclos for representa o z e a variavel i o x

        //VARIAVEL Z
        for (int j = 0; j < sizeOfTerrain; j++) {
            
            //VARIAVEL X
            for (int i = 0; i < sizeOfTerrain; i++) {
                Instantiate(grass, new Vector3(i * start_point, 0, j * start_point), grass.transform.rotation);

                //Acrescenta árvores ao mapa para que o mapa não pareca que acabe de repente
                if (i < start_point || j < start_point || i >= sizeOfTerrain-start_point || j >= sizeOfTerrain-start_point)
                {
                    Instantiate(tree1, new Vector3(i * 4, 0, j * 4), tree1.transform.rotation);
                    //Instantiate(stone, new Vector3(i * 4, 0, j * 4), stone.transform.rotation);
                    terrain[i, j] = tree_key;
                }
            }
        }

        //Definir a posição do macdonalds e um raio que o macdonalds ocupa no terreno
        int x_random_pos_mac = Random.Range(70, 90);
        int z_random_pos_mac = Random.Range(70, 90);

        Instantiate(macdonalds, new Vector3(x_random_pos_mac * 4, 1, z_random_pos_mac * 4), macdonalds.transform.rotation);

        //VARIAVEL X
        for(int i = -4; i <=4; i++)
        {
            //VARIAVEL Z
            for(int j = -3; j <= 3; j++)
            {
                terrain[x_random_pos_mac + i, z_random_pos_mac + j] = macdonalds_key;
            }
        }

        //TESTES - VERTICES DE MARGEM SEM ARVORES PARA A AREA EM REDOR AO MACDONALDS
        //Instantiate(tree1, new Vector3((x_random_pos_mac - 4) * 4, 0, (z_random_pos_mac - 3) * 4), tree1.transform.rotation);
        //Instantiate(tree1, new Vector3((x_random_pos_mac - 4) * 4, 0, (z_random_pos_mac + 3) * 4), tree1.transform.rotation);
        //Instantiate(tree1, new Vector3((x_random_pos_mac + 4) * 4, 0, (z_random_pos_mac - 3) * 4), tree1.transform.rotation);
        //Instantiate(tree1, new Vector3((x_random_pos_mac + 4) * 4, 0, (z_random_pos_mac + 3) * 4), tree1.transform.rotation);

        //Distancia entre o jogador e o macdonalds
        int distX = Mathf.Abs(posX - x_random_pos_mac) + 1;
        int distZ = Mathf.Abs(posZ - z_random_pos_mac) + 1;

        //Cria entre 1 a 3 caminhos do jogador até ao Macdonalds
        int n_path = Random.Range(1, 3);

        for (int c = 0; c<n_path; c++)
        {
            //POSIÇAO ATUAL DO JOGADOR A CADA MOVIMENTACAO
            int walk_x = posX-1;
            int walk_z = posZ-1;

            //NUMERO DE PACOS QUE DA A CADA MOVIMENTACAO
            int totalStepsX = 0;
            int totalStepsZ = 0;

            //NUMERO DE PACOS QUE DEU NO TOTAL
            int numberOfStepsX = 0;
            int numberOfStepsZ = 0;

            while (terrain[walk_x, walk_z] != macdonalds_key 
                && terrain[walk_x-1, walk_z] != macdonalds_key
                 && terrain[walk_x+1, walk_z] != macdonalds_key
                  && terrain[walk_x, walk_z-1] != macdonalds_key
                   && terrain[walk_x, walk_z+1] != macdonalds_key
                    && terrain[walk_x+1, walk_z+1] != macdonalds_key
                     && terrain[walk_x+1, walk_z-1] != macdonalds_key
                      && terrain[walk_x-1, walk_z+1] != macdonalds_key
                       && terrain[walk_x-1, walk_z-1] != macdonalds_key)
            {
                
                if (totalStepsX < distX) { 
                    numberOfStepsX = Random.Range(1, distX - totalStepsX);

                    for(int x = 0; x < numberOfStepsX; x++)
                    {
                        if(terrain[walk_x + x + 1, walk_z] == nothing_key)
                            terrain[walk_x + x + 1, walk_z] = path_key;
                        if (terrain[walk_x + x + 1, walk_z + 1] == nothing_key)
                            terrain[walk_x + x + 1, walk_z + 1] = path_key;
                        if (terrain[walk_x + x + 1, walk_z - 1] == nothing_key)
                            terrain[walk_x + x + 1, walk_z - 1] = path_key;
                    }

                    walk_x = walk_x + numberOfStepsX;
                    totalStepsX = totalStepsX + numberOfStepsX;
                }

                if (totalStepsZ < distZ) { 
                    numberOfStepsZ = Random.Range(1, distZ - totalStepsZ);

                    for (int z = 0; z < numberOfStepsZ; z++)
                    {
                        if (terrain[walk_x, walk_z + z + 1] == nothing_key)
                            terrain[walk_x, walk_z + z + 1] = path_key;
                        if (terrain[walk_x + 1, walk_z + z + 1] == nothing_key)
                            terrain[walk_x + 1, walk_z + z + 1] = path_key;
                        if (terrain[walk_x - 1, walk_z + z + 1] == nothing_key)
                            terrain[walk_x - 1, walk_z + z + 1] = path_key;
                    }

                    walk_z = walk_z + numberOfStepsZ;
                    totalStepsZ = totalStepsZ + numberOfStepsZ;
                }
            }
        }

        //Cria as árvores aleatoriamente criando o resto do mapa

        int random_trees;
        int random_pos;
        
        int[] possible_enemy_x = new int[500];
        int[] possible_enemy_z = new int[500];
        int count_enemy = 0;

        //VARIAVEL X
        for (int j = start_point; j < sizeOfTerrain; j++)
        {
            random_trees = Random.Range(70, 80);
            
            //VARIAVEL Z
            for (int i = start_point; i < random_trees; i++)
            {
                if(j == start_point || j == start_point+1)
                    random_pos = Random.Range(5, 96);
                else
                    random_pos = Random.Range(5, 96);

                if (terrain[random_pos, j] == nothing_key)
                {
                    Instantiate(tree1, new Vector3(random_pos * 4, 0, j * 4), tree1.transform.rotation);
                    terrain[random_pos,j] = tree_key;

                    
                }else if (terrain[random_pos, j] == path_key && (i>3 || j>3)) {
                    if (count_enemy < 500)
                    {
                        possible_enemy_x[count_enemy] = random_pos;
                        possible_enemy_z[count_enemy++] = j;
                    }
                }
            }
        }

        for (int e = 0; e < max_enemy; e++)
        {
            int random_enemy = Random.Range(0, count_enemy);
            int random_type_enemy = Random.Range(0, 2);

            if(random_type_enemy == 0)
                Instantiate(enemy, new Vector3(possible_enemy_x[random_enemy] * 4, 0, possible_enemy_z[random_enemy] * 4), enemy.transform.rotation);
            else
                Instantiate(roller, new Vector3(possible_enemy_x[random_enemy] * 4, 2, possible_enemy_z[random_enemy] * 4), roller.transform.rotation);

            terrain[possible_enemy_x[random_enemy], possible_enemy_z[random_enemy]] = enemy_key;
        }

        for (int h = 0; h < max_happy_meals; h++)
        {

            int random_enemy = Random.Range(0, count_enemy);
            if (terrain[possible_enemy_x[random_enemy], possible_enemy_z[random_enemy]] == path_key) {
                Instantiate(happy_meal, new Vector3(possible_enemy_x[random_enemy] * 4, 0, possible_enemy_z[random_enemy] * 4), happy_meal.transform.rotation);
                terrain[possible_enemy_x[random_enemy], possible_enemy_z[random_enemy]] = happy_meal_key;
            }
            else {
                h--;
            }
        }




    }

    void Update()
    {
        if(terrain[(int) player.transform.position.x/4, (int) player.transform.position.z/4] == macdonalds_key)
        {
            SceneManager.LoadScene("Finish");
        }
    }
	
}
